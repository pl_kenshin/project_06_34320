## Instrukcja obsługi

Po uruchomieniu programu wyświetla się ekran początkowy aplikacji, na którym znajduje się krótki opis zasad Quizu oraz przycisk służący do rozpoczęcia go. Po naciśnięciu przycisku "Rozpocznij" wyświetla się okno pytań i odpowiedzi. Na tym ekranie należy wybierać jedną odpowiedź, a następnie naciskać przycisk "Następne pytanie" żeby przechodzić między kolejnymi pytaniami. W przypadku ostatniego pytania wyświetlony jest przycisk "Zakończ test", który umożliwia zakończenie testu i zapisanie wyników. Po naciśnięciu ww. przycisku wyświetla się okno podsumowania. W tym oknie można sprawdzić uzyskać informacje o swoim podejściu do testu: liczba uzyskanych punktów, ocena i status zaliczenia, a także szczegółowe informacje dotyczące udzielonych i poprawnych odpowiedzi. Na dole okna znajduje się także przycisk umożliwiający powtórzenie całego testu.

## Wykorzystane technologie

1. Język programowania: Java jdk 17
2. Narzędzie automatyzujące budowę oprogramowania Maven
3. Platforma oprogramowania JavaFX
4. Środowisko programistyczne IntelliJ IDEA 2021.3.3.0

## Podział pracy

Mateusz Ptak  | Szymon Stachura
------------- | -------------
App.java | Dokumentacja JavaDOC
MainController.java | ResultsController.java
QuestionsListGenerator.java | ResultsGenerator.java
QuestionsController.java | UserAnswer.java
resources | Question.java
module-info.java | pom.xml
README.md | hotfix